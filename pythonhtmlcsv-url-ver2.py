import urllib
import csv

url = """

http://www.ranker.com/list/full-list-of-toyota-models/reference?var=13&utm_expid=16418821-253.Q8hRDqTzQmC6TeDxwzEYhw.2&utm_referrer=http%3A%2F%2Fwww.ranker.com%2Flist%2Ffull-list-of-toyota-models%2Freference%3Fpage%3D2

"""
f = urllib.urlopen(url)
html_doc = f.read()




from bs4 import BeautifulSoup
soup = BeautifulSoup(html_doc, 'html.parser')
# print soup.prettify()

list_ele = soup.select("#listBody #mainListCnt ol > li.blog ")
cars = []

for ele_tag in list_ele:
	car = {}

	car_name = ele_tag.select('.name > div > h2 > a')
	if car_name:
		car_name = car_name[0].string
		car_name = str(car_name)
		
	car_info_ele = ele_tag.select('.float > .blogText')
	if car_info_ele:
		car_info_ele = car_info_ele[0]
		car_description = car_info_ele.select('.blogTextWiki')

		if car_description:
			car_description = car_description[0].get_text()
			car_description = unicode(car_description)

		car_make = car_info_ele.select('.propText > ul > li')
		if car_make and len(car_make) > 0:
			car_make  = car_make[0].get_text()
			car_make = car_make[6:]
			car_make = str(car_make)
		

		car_class = car_info_ele.select('.propText > ul > li')
		if car_class and len(car_class) > 1:
			car_class = car_class[1].get_text()
			car_class = car_class[6:]
			car_class = str(car_class)


	car_img_ele = ele_tag.select('.float > div > figure > span > a > img')
	if car_info_ele and len(car_img_ele) > 0:
		car_img = car_img_ele[0]['src']

	if (car_name):
		car['car_name'] = car_name
	else:
		car['car_name'] = ''

	if (car_description):
		car['car_description'] = car_description
	else:
		car['car_description'] = ''

	if (car_make):
		car['car_make'] = car_make
	else:
		car['car_make'] = car_make
	
	if (car_class):
		car['car_class'] = car_class
	else:
		car['car_class'] += ''

	if (car_img):
		car['car_img'] = car_img
	else:
		car['car_img'] = ''

	# car_item = [car['car_name'], car['car_description'], car['car_make'], car['car_class'], car['car_img']]
	car_item = [car['car_name'], car['car_make'], car['car_class'], car['car_img']]
	cars += car_item


	print car, '\n\n'

# print cars


with open('/home/thinh/Desktop/filecsv/test.csv', 'wb') as f:
	writer = csv.writer(f, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
	writer.writerows([cars])




print "List length: ", len(list_ele)


